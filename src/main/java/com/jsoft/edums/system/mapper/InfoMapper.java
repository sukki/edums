package com.jsoft.edums.system.mapper;

import java.util.List;

import com.jsoft.edums.common.annotation.MyBatisDao;
import com.jsoft.edums.common.entity.Page;
import com.jsoft.edums.system.entity.Info;

@MyBatisDao
public interface InfoMapper {
	List<Info> listPageInfo(Page page);
}
