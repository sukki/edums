package com.jsoft.edums.system.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jsoft.edums.base.service.BaseService;
import com.jsoft.edums.common.entity.Page;
import com.jsoft.edums.system.entity.Info;
import com.jsoft.edums.system.mapper.InfoMapper;
import com.jsoft.edums.system.service.InfoService;

@Service
public class InfoServiceImpl extends BaseService implements InfoService{
	
	@Autowired
	private InfoMapper infoMapper;
	
	public List<Info> listPageInfo(Page page) {
		// TODO Auto-generated method stub
		return infoMapper.listPageInfo(page);
	}

}
