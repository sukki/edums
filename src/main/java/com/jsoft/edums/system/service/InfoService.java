package com.jsoft.edums.system.service;

import java.util.List;

import com.jsoft.edums.common.entity.Page;
import com.jsoft.edums.system.entity.Info;

public interface InfoService {
	List<Info> listPageInfo(Page page);
}
